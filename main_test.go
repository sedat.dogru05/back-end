package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
)


func TestGetAllTodo(t *testing.T)  {
	r := gin.Default()

	r.GET("/api/getall", func(c *gin.Context) {})

	ts := httptest.NewServer(r)
	defer ts.Close()

	resp, err := http.Get(fmt.Sprintf("%s/api/getall", ts.URL))

	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}
}

func TestAddTodo(t *testing.T)  {
	r := gin.Default()

	r.POST("/api/add", func(c *gin.Context) {	})

	w := httptest.NewRecorder()

	todoPayload := getTodoPOSTPayload()
	req, _ := http.NewRequest("POST", "/api/add", strings.NewReader(todoPayload))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(todoPayload)))

	r.ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Fail()
	}
}

func getTodoPOSTPayload() string {
	params := url.Values{}
	params.Add("title", "wake up at 7 am")
	return params.Encode()
}


